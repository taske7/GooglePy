GooglePy
====================

This python script search/google certain keywords, crawls the webpages from the results, and return all emails found.

## Installation:
It is recomended to use via pyenv We will be supporting python 3.6.0 and above going forward

```
pip install --upgrade pip
curl https://pyenv.run | bash
curl -L https://github.com/pyenv/pyenv-installer/raw/master/bin/pyenv-installer | bash
pyenv local 3.6.0
pip install -r requirements.txt
```

##  Download APIs:
  - [crawl emails](#email-crawler)

### crawl emails

Start the search with a keyword. We use "iphone developers" as an example.

```bash
    python email_crawler.py "iphone developers"
```

The emails will be saved in ./data/iphone_developers_emails.csv
